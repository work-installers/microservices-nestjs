import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class AppService {
  constructor(@Inject('MAIL_SERVICE') private clientMail: ClientProxy) {}
  getHello(): string {
    return 'Hello World!';
  }

  async newEmail(data: object) {
    // this.clientMail.emit('new_email', data);

    return this.clientMail.send('new_email', data);
  }
}
