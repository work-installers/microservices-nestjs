import { NestFactory } from '@nestjs/core';
import { Transport, MicroserviceOptions } from '@nestjs/microservices';
import { MailModule } from './mail.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    MailModule,
    {
      transport: Transport.TCP,
      options: {
        host: 'localhost',
        port: 8001,
      },
    },
  );
  await app.listen().then(() => {
    console.log('Microservice MailModule is running on: http://localhost:8001');
  });
}
bootstrap();
