import { Controller } from '@nestjs/common';
import { EventPattern, MessagePattern } from '@nestjs/microservices';
import { MailService } from './mail.service';

@Controller()
export class MailController {
  constructor(private readonly mailService: MailService) {}

  // @Get()
  // getHello(): string {
  //   return this.mailService.getHello();
  // }

  @MessagePattern('new_email')
  async handleEmail(data: any) {
    console.log('data capturada', data);

    return this.mailService.getHello();
  }
}
