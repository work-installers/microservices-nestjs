import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DB } from './database/config/DB.module';
import { Schemas } from './database/schemas/index';
import { MailController } from './mail.controller';
import { MailService } from './mail.service';

@Module({
  imports: [
    TypeOrmModule.forRoot(DB.connections as object),
    TypeOrmModule.forFeature(Schemas),
  ],
  controllers: [MailController],
  providers: [MailService],
})
export class MailModule {}
