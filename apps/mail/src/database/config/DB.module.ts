export const DB = {
  connections: {
    type: 'oracle',
    host: '192.168.1.35',
    port: 1521,
    username: 'system',
    password: '123',
    database: 'ORCLPDB1',
    serviceName: 'ORCLPDB1',
    schema: 'DATABASE_ORACLE',
    autoLoadEntities: true,
    // Colocar siempre en false
    // synchronize: false,
  },
};
