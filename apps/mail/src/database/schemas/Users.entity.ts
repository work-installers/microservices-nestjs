import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'TEST' })
export class Users {
  @PrimaryGeneratedColumn()
  ID: number;

  @Column({ type: 'varchar', length: 250, nullable: false })
  NOMBRE: string;
  @Column({ type: 'varchar', length: 250, nullable: false })
  APELLIDOS: string;
}

// import { EntitySchema } from 'typeorm';

// export const Users = new EntitySchema({
//   name: 'mnt_users',
//   columns: {
//     id: {
//       type: Number,
//       primary: true,
//       generated: true,
//     },
//     username: {
//       type: String,
//       length: 250,
//       unique: true,
//       nullable: false,
//     },
//     password: {
//       type: 'text',
//       nullable: false,
//     },
//     last_login: {
//       type: Date,
//       default: () => 'CURRENT_TIMESTAMP',
//     },
//     is_suspended: {
//       type: Boolean,
//       default: false,
//     },
//     created_at: {
//       type: Date,
//       default: () => 'CURRENT_TIMESTAMP',
//     },
//   },
// });
