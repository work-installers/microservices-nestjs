import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Users } from './database/schemas/index';

@Injectable()
export class MailService {
  constructor(
    @InjectRepository(Users) private userRepository: Repository<any>,
  ) {}

  getHello(): string {
    const finAllUsers = this.userRepository.find();

    // return 'Send from Microservice Email';
    return finAllUsers as any;
  }
}
